using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Waranya.GameDev3.Chapter2
{
    public class ControlObjectMovementOnXZPlaneUsingArrowKeys : StepMovement
    {
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                MoveLeft();
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                MoveRight();
            }
            else if (Input.GetKey(KeyCode.UpArrow))
            {
                MoveForward();
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                MoveBackward();
            }
        }
    }
}