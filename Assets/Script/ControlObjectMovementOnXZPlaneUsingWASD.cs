using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Waranya.GameDev3.Chapter2
{
    public class ControlObjectMovementOnXZPlaneUsingWASD : StepMovement
    {
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                MoveLeft();
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                MoveRight();
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                MoveForward();
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                MoveBackward();
            }
        }
    }
}