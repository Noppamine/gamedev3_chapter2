using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

namespace Waranya.GameDev3.Chapter2
{
    public class BasicMouseInformation : MonoBehaviour
    {
        // Start is called before the first frame update
        public Text m_TextMousePosition;

        // Update is called once per frame
        void Update()
        {
            m_TextMousePosition.text = Input.mousePosition.ToString();
        }
    }
}